#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <pthread.h>
#include <fcntl.h>
#include "job_scheduler.h"

jobStructure js[1000];
queueStructure *q;       

int maxJobs, jobCounter;    

int main(int argc, char **argv)
{ 
    if (argc != 2)
    {
        printf("Usage: %s jobs\n", argv[0]);
        exit(0);
    }

    pthread_t tid;
    maxJobs = atoi(argv[1]);
    printf("Jobs to run at the same time: %d\n", maxJobs);
    
    
    q = queue_init(500);
    pthread_create(&tid, NULL, run_threads, NULL);

    int i = 0;    
    size_t len = 1000;
    char line[len],org_line[1000];
    char *tok;           
    char *command = malloc(1000*sizeof(char));
    while (printf("job_sh> "))
    {   if(getInput(line, len) != -1)
        {
            strcpy(org_line,line);
            if ((tok = strtok(line, " \n")) != NULL)
            {
                if (strcmp(tok, "submit") == 0)
                {
                        char* strng = strstr(org_line, "submit") + 6;
                        jobStructure j;

                        int k;
                        k = 0;
                        while ((strng[k]==' ')||(strng[k]=='\n'))
                            ++k;

                        strcpy(command,strng+k);

                        j.jobId = i;
                        j.command = malloc(1000*sizeof(char));
                        strcpy(j.command , command);
                        j.status = "WAITING";
                        j.errorStatus = -1;
                        j.startTime = j.endTime = NULL;
                        sprintf(j.fnout, "%d.out", j.jobId);
                        sprintf(j.fnerr, "%d.err", j.jobId);
                        js[i] = j;

                        queue_insert(q, js+i);
                        i++;
                }
                else if (strcmp(tok, "submithistory") == 0 || strcmp(tok, "showjobs") == 0){
                    int c;
                    if(js != NULL && i != 0)
                    {
                        if(strcmp(tok, "showjobs") == 0)
                        {
                            printf("Job ID\tCommand\tStatus\t\n");
                            for (c = 0; c < i; ++c)
                            {
                                if (strcmp(js[c].status, "COMPLETE") != 0)
                                    printf("%d\t%s\t%s\n",js[c].jobId,js[c].command,js[c].status);
                            }
                        }
                        else if (strcmp(tok, "submithistory") == 0)
                        {
                            printf("Job ID\tCommand\tStart Time\tStop Time\tExit Status\t\n");
                            for (c = 0; c < i; ++c)
                            {
                                if (strcmp(js[c].status, "COMPLETE") == 0)
                                    printf("%d\t%s\t%s\t%s\t%d\t\n",js[c].jobId,js[c].command,js[c].startTime,js[c].endTime,js[c].errorStatus);
                            }
                        }
                    }
                }
            }
        }
    }

}

void *run_threads(void *arg)
{
    

    jobCounter = 0;
    pthread_t tid;
    while(1)
    {
        if (q->count > 0 && jobCounter < maxJobs)
        {
            pthread_create(&tid, NULL, run_command, NULL);
            pthread_detach(tid);
        }
        
        sleep(1); 
    }
    
    return NULL;
}


void *run_command(void *arg)
{
    jobStructure *jp = queue_delete(q);     
    char **args; 
    pid_t pid;   

    ++jobCounter;
    jp->status = "RUNNING";
    jp->startTime = get_time();

    pid = fork();
    if (pid == 0) 
    {
        int fd1 = open(jp->fnout, O_CREAT | O_APPEND | O_WRONLY, 0755);
        int fd2 = open(jp->fnerr, O_CREAT | O_APPEND | O_WRONLY, 0755);

        dup2(fd1, STDOUT_FILENO); 
        dup2(fd2, STDERR_FILENO); 
        char *copy = malloc(sizeof(char) * (strlen(jp->command) + 1));
        strcpy(copy, jp->command);

        char *arg;
        args = malloc(sizeof(char *));
        int i = 0;
        while ((arg = strtok(copy, " ")) != NULL)
        {
            args[i] = malloc(sizeof(char) * (strlen(arg) + 1));
            strcpy(args[i], arg);
            args = realloc(args, sizeof(char *) * (++i + 1));
            copy = NULL;
        }
        args[i] = NULL;

        execvp(args[0], args);
        fprintf(stderr, "Error: command execution failed for \"%s\"\n", args[0]);
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    else if (pid > 0) 
    {
        --jobCounter;

        waitpid(pid, &jp->errorStatus, WUNTRACED);
        jp->status = "COMPLETE";
        jp->endTime = get_time();
    }
    else
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }


    return NULL;
}


queueStructure *queue_init(int n)
{
    queueStructure *q = malloc(sizeof(queueStructure));
    q->size = n;
    q->buffer = malloc(sizeof(jobStructure *) * n);
    q->startTime = 0;
    q->end = 0;
    q->count = 0;

    return q;
}


int queue_insert(queueStructure *q, jobStructure *jp)
{
    if ((q == NULL) || (q->count == q->size))
        return -1;

    q->buffer[q->end % q->size] = jp;
    q->end = (q->end + 1) % q->size;
    ++q->count;

    return q->count;
}


jobStructure *queue_delete(queueStructure *q)
{
    if ((q == NULL) || (q->count == 0))
        return (jobStructure *)-1;

    jobStructure *j = q->buffer[q->startTime];
    q->startTime = (q->startTime + 1) % q->size;
    --q->count;

    return j;
}


void queue_destroy(queueStructure *q)
{
    free(q->buffer);
    free(q);
}


char *getTemp(char *s)
{
    char *temporary;

    int i=-1,c=0;
    temporary = malloc(1 * strlen(s) * sizeof(char));
    while ((c = s[++i]) != '\0')
        temporary[i] = c;
    temporary[i] = '\0';

    return temporary;
}

char *get_time()
{
    time_t tim = time(NULL);
    char *time = ctime(&tim);
    time[strlen(time)-1] = '\0';
    return time;
}

int getInput(char *s, int n)
{
    int i = 0, c;
    while(i<n-1 && (c = getchar()) != '\n')
    {
        if (c == EOF)
            return -1;
        s[i] = c;
        i++;
    }
    s[i] = '\0';
    return i;
}
