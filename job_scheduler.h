typedef struct jobStructure
{
    int jobId;        
    char *command; 
    char *startTime;    
    char *endTime;      
    char *status;     
    int errorStatus;  
    pthread_t tid;  
    char fnout[10]; 
    char fnerr[10]; 
} jobStructure;

typedef struct queueStructure
{
    int size;     
    jobStructure **buffer; 
    int startTime;    
    int end;      
    int count;    
} queueStructure;


void *run_command(void *arg);
void *run_threads(void *arg);


jobStructure create_job(char *command, int jobId);
void list_jobs(jobStructure *jobs, int n, char *mode);


queueStructure *queue_init(int n);
int queue_insert(queueStructure *q, jobStructure *jp);
jobStructure *queue_delete(queueStructure *q);
void queue_destroy(queueStructure *q);


char *getTemp(char *s);
char *get_time();

int getInput(char *s, int n);